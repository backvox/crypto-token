<?php
/**
 * @link https://github.com/Aikrof
 * @package Voxonics\Crypto
 * @author Denys <AikrofStark@gmail.com>
 */

declare(strict_types=1);

namespace Voxonics\Crypto;

/**
 * Class Signature
 */
class Signature
{
    /**
     * The resultant hash
     *
     * @var string|null
     */
    protected $hash;

    /**
     * Hash constructor.
     *
     * @param string|null $hash
     */
    public function __construct(string $hash = null)
    {
        if ($hash !== null) {
            $this->setHash($hash);
        }
    }

    /**
     * @return string|null
     */
    public function getHash(): ?string
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     *
     * @return static
     */
    public function setHash(string $hash): self
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Returns the current hash as a string representation of the signature
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->hash;
    }
}