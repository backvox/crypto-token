<?php
/**
 * @link https://github.com/Aikrof
 * @package Voxonics\Crypto
 * @author Denys <AikrofStark@gmail.com>
 */

declare(strict_types=1);

namespace Voxonics\Crypto;

/**
 * Class CryptoConfig
 *
 * Basic config for Crypto key
 */
class CryptoConfig
{
    /**
     * @var string
     */
    protected $algorithm;

    /**
     * @var string
     */
    protected $key;

    /**
     * CryptoConfig constructor.
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->init($config);
    }

    /**
     * Base config for Crypto auth
     *
     * @param array $config
     */
    protected function init(array $config): void
    {
        $this
            ->setAlgorithm($config['alg'] ?? $_SERVER['CRYPTO_ALG'] ?? 'HS512')
            ->setKey($config['key'] ?? $_SERVER['CRYPTO_KEY'] ?? '');
    }

    /**
     * @return string
     */
    public function getAlgorithm(): string
    {
        return $this->algorithm;
    }

    /**
     * @param string $algorithm
     *
     * @return static
     */
    public function setAlgorithm(string $algorithm): self
    {
        $this->algorithm = $algorithm;

        return $this;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @param string $key
     *
     * @return static
     */
    public function setKey(string $key): self
    {
        $this->key = $key;

        return $this;
    }
}