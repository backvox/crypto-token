<?php
/**
 * @link https://github.com/Aikrof
 * @package Voxonics\Crypto
 * @author Denys <AikrofStark@gmail.com>
 */

declare(strict_types=1);

namespace Voxonics\Crypto;

use Voxonics\Crypto\Exceptions\InvalidAlgorithmException;
use Voxonics\Crypto\Signer\Key;
use Voxonics\Crypto\Signer\KeyInterface;
use Voxonics\Crypto\Signer\SignerInterface;

/**
 * Class CryptoService
 */
class CryptoService
{
    /**
     * @var CryptoConfig
     */
    private $cryptoConfig;

    /**
     * Supported algorithms
     */
    public const ALGORITHMS = [
        'HS256' => \Voxonics\Crypto\Signer\Hmac\Sha256::class,
        'HS384' => \Voxonics\Crypto\Signer\Hmac\Sha384::class,
        'HS512' => \Voxonics\Crypto\Signer\Hmac\Sha512::class,
    ];

    /**
     * CryptoService constructor.
     *
     * @param CryptoConfig|null $cryptoConfig
     */
    public function __construct(CryptoConfig $cryptoConfig = null)
    {
        $this->cryptoConfig = $cryptoConfig ?: new CryptoConfig();
    }

    /**
     * Get Crypto config.
     *
     * @return CryptoConfig
     */
    public function getCryptoConfig(): CryptoConfig
    {
        return $this->cryptoConfig;
    }

    /**
     * Get all supported algorithms.
     *
     * @return array
     */
    public static function getSupportedAlgs(): array
    {
        return array_keys(self::ALGORITHMS);
    }

    /**
     * Check if algorithm supported
     *
     * @param string $algorithm
     *
     * @return bool
     */
    public static function isAlgSupported(string $algorithm): bool
    {
        return (bool) \in_array($algorithm, self::getSupportedAlgs(), true);
    }

    /**
     * Generate crypto signature
     *
     * @param array         $payload signature payload data
     * @param string|null   $secretKey signature secret
     *
     * @return Signature
     *
     * @throws InvalidAlgorithmException
     */
    public function generateSignature(array $payload, string $secretKey = null): Signature
    {
        /** @var Builder $builder */
        $builder = $this
            ->getBuilder($secretKey)
            ->withClaims($payload);

        return $builder->getSignature();
    }

    /**
     * Validate signature
     *
     * @param Signature     $expected
     * @param array         $payload
     * @param string|null   $secretKey
     *
     * @return bool
     * @throws InvalidAlgorithmException
     */
    public function validate(Signature $expected, array $payload, string $secretKey = null): bool
    {
        /** @var Builder $builder */
        $builder = $this
            ->getBuilder($secretKey)
            ->withClaims($payload);

        return $builder->verify((string) $expected);
    }

    /**
     * Get Builder instance
     *
     * @param string|null $secretKey
     *
     * @return Builder
     *
     * @throws InvalidAlgorithmException
     */
    protected function getBuilder(string $secretKey = null): Builder
    {
        $config = $this->getCryptoConfig();

        if ($secretKey !== null) {
            $config->setKey($secretKey);
        }

        $algorithm = $config->getAlgorithm();
        if (!self::isAlgSupported($algorithm)) {
            throw new InvalidAlgorithmException(
                'Algorithm: ' . $algorithm . ' is not supported.'
            );
        }

        $algorithmClass = self::ALGORITHMS[$algorithm];

        /** @var SignerInterface $signer */
        $signer = new $algorithmClass;
        /** @var KeyInterface $key */
        $key = new Key($config->getKey());

        return new Builder($signer, $key);
    }
}
