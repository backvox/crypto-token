<?php
/**
 * @link https://github.com/Aikrof
 * @package Voxonics\Crypto
 * @author Denys <AikrofStark@gmail.com>
 */

declare(strict_types=1);

namespace Voxonics\Crypto;

use RuntimeException;
use Voxonics\Crypto\Signer\Key;
use Voxonics\Crypto\Signer\KeyInterface;
use Voxonics\Crypto\Signer\SignerInterface;

/**
 * Class Builder
 *
 * This class makes easier the hash creation process
 */
class Builder
{
    /**
     * @var SignerInterface|null
     */
    protected $signer;

    /**
     * @var KeyInterface|null
     */
    protected $key;

    /**
     * @var array
     */
    protected $claims = [];

    /**
     * Builder constructor.
     *
     * @param SignerInterface|null  $signer
     * @param Key|null              $key
     */
    public function __construct(SignerInterface $signer = null, KeyInterface $key = null)
    {
        $this->signer = $signer;
        $this->key = $key;
    }

    /**
     * Configures a claim items
     *
     * @param array $claims
     *
     * @return static
     */
    public function withClaims(array $claims): self
    {
        foreach ($claims as $name => $value) {
            $this->withClaim($name, $value);
        }

        return $this;
    }

    /**
     * Configures a claim item
     *
     * @param string|int $name
     * @param mixed      $value
     *
     * @return static
     */
    public function withClaim($name, $value): self
    {
        if (\is_string($name) || \is_int($name)) {
            $this->claims[$name] = $value;
        }

        return $this;
    }

    /**
     * Returns the resultant signature
     *
     * @param SignerInterface|null $signer
     * @param KeyInterface|null $key
     *
     * @return Signature
     */
    public function getSignature(SignerInterface $signer = null, KeyInterface $key = null): Signature
    {
        $signer = $signer ?: $this->signer;
        $key = $key ?: $this->key;

        /**
         * Generated hash signature
         *
         * @var string
         */
        $signature = $this->createSignature(
            $this->jsonEncode($this->claims),
            $signer,
            $key
        );

        return new Signature($signature);
    }

    /**
     * @param string                $payload
     * @param SignerInterface|null  $signer
     * @param KeyInterface|null     $key
     *
     * @return string|null
     */
    protected function createSignature(
        string $payload,
        SignerInterface $signer = null,
        KeyInterface $key = null
    ): ?string {
        if ($signer === null || $key === null) {
            return null;
        }

        return $signer->createHash($payload, $key);
    }

    /**
     * Verify given expected hash with new generated hash
     *
     * @param string                $expected
     * @param SignerInterface|null  $signer
     * @param KeyInterface|null     $key
     *
     * @return bool
     */
    public function verify(string $expected, SignerInterface $signer = null, KeyInterface $key = null): bool
    {
        $signer = $signer ?: $this->signer;
        $key = $key ?: $this->key;

        if ($signer === null || $key === null) {
            return false;
        }

        return $signer->verify($expected, $this->jsonEncode($this->claims), $key);
    }

    /**
     * Check expected and generated hashes are equals
     *
     * @param string                $expected
     * @param string                $generated
     * @param SignerInterface|null  $signer
     *
     * @return bool
     */
    public function verifyHash(string $expected, string $generated, SignerInterface $signer = null): bool
    {
        $signer = $signer ?: $this->signer;

        return $signer !== null && $signer->hashEquals($expected, $generated);
    }

    /**
     * Encodes to JSON, validating the errors
     *
     * @param mixed $data
     *
     * @return string
     *
     * @throws RuntimeException When something goes wrong while encoding
     */
    protected function jsonEncode($data): string
    {
        $json = json_encode($data);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new RuntimeException('Error while encoding to JSON: ' . json_last_error_msg());
        }

        return $json;
    }
}
