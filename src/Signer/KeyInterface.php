<?php
/**
 * @link https://github.com/Aikrof
 * @package Lcobucci\JWT\Signer
 * @author Denys <AikrofStark@gmail.com>
 */

declare(strict_types=1);

namespace Voxonics\Crypto\Signer;

/**
 * Interface KeyInterface
 */
interface KeyInterface
{
    /**
     * @param string $content
     *
     * @return static
     */
    public function setContent(string $content): self;

    /**
     * @return string
     */
    public function getContent(): string;
}