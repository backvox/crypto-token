<?php
/**
 * @link https://github.com/Aikrof
 * @package Voxonics\Crypto\Signer
 * @author Denys <AikrofStark@gmail.com>
 */

declare(strict_types=1);

namespace Voxonics\Crypto\Signer;

/**
 * Base class for hmac signers
 */
abstract class Hmac implements SignerInterface
{
    /**
     * {@inheritDoc}
     *
     * @param string        $payload
     * @param KeyInterface  $key
     *
     * @return string
     *
     */
    public function createHash(string $payload, KeyInterface $key): string
    {
        return \hash_hmac($this->getAlgorithm(), $payload, $key->getContent());
    }

    /**
     * {@inheritDoc}
     *
     * @param string        $expected
     * @param string        $payload
     * @param KeyInterface  $key
     *
     * @return boolean
     */
    public function verify(string $expected, string $payload, KeyInterface $key): bool
    {
        if (!\is_string($expected)) {
            return false;
        }

        return $this->hashEquals($expected, $this->createHash($payload, $key));
    }

    /**
     * {@inheritDoc}
     *
     * @param string $generated
     *
     * @return boolean
     *
     */
    public function hashEquals(string $expected, string $generated): bool
    {
        $expectedLength = \strlen($expected);

        if ($expectedLength !== \strlen($generated)) {
            return false;
        }

        $res = 0;

        for ($i = 0; $i < $expectedLength; ++$i) {
            $res |= \ord($expected[$i]) ^ \ord($generated[$i]);
        }

        return $res === 0;
    }
}
