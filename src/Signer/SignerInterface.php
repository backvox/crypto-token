<?php
/**
 * @link https://github.com/Aikrof
 * @package Lcobucci\JWT\Signer
 * @author Denys <AikrofStark@gmail.com>
 */

declare(strict_types=1);

namespace Voxonics\Crypto\Signer;

/**
 * Interface SignerInterface
 */
interface SignerInterface
{
    /**
     * Creates a hash with the given data
     *
     * @param string        $payload
     * @param KeyInterface  $key
     *
     * @return string
     */
    public function createHash(string $payload, KeyInterface $key): string;

    /**
     * Performs the signature verification
     *
     * @param string        $expected
     * @param string        $payload
     * @param KeyInterface  $key
     *
     * @return boolean
     */
    public function verify(string $expected, string $payload, KeyInterface $key): bool;

    /**
     * Hash equal
     *
     * @internal
     *
     * @param string $expected
     * @param string $generated
     *
     * @return boolean
     */
    public function hashEquals(string $expected, string $generated): bool;

    /**
     * Return the algorithm id
     *
     * @return string
     */
    public function getAlgorithmId(): string;

    /**
     * Return the algorithm name
     *
     * @return string
     */
    public function getAlgorithm(): string;
}