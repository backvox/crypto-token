<?php
/**
 * @link https://github.com/Aikrof
 * @package Voxonics\Crypto\Signer\Hmac
 * @author Denys <AikrofStark@gmail.com>
 */

declare(strict_types=1);

namespace Voxonics\Crypto\Signer\Hmac;

use Voxonics\Crypto\Signer\Hmac;

/**
 * Signer for HMAC SHA-256
 */
class Sha256 extends Hmac
{
    /**
     * {@inheritdoc}
     */
    public function getAlgorithmId(): string
    {
        return 'HS256';
    }

    /**
     * {@inheritdoc}
     */
    public function getAlgorithm(): string
    {
        return 'sha256';
    }
}
