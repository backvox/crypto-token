<?php
/**
 * @link https://github.com/Aikrof
 * @package Voxonics\Crypto\Signer\Hmac
 * @author Denys <AikrofStark@gmail.com>
 */

declare(strict_types=1);

namespace Voxonics\Crypto\Signer\Hmac;

use Voxonics\Crypto\Signer\Hmac;

/**
 * Signer for HMAC SHA-384
 */
class Sha384 extends Hmac
{
    /**
     * {@inheritdoc}
     */
    public function getAlgorithmId(): string
    {
        return 'HS384';
    }

    /**
     * {@inheritdoc}
     */
    public function getAlgorithm(): string
    {
        return 'sha384';
    }
}
