<?php
/**
 * @link https://github.com/Aikrof
 * @package Voxonics\Crypto\Signer
 * @author Denys <AikrofStark@gmail.com>
 */

declare(strict_types=1);

namespace Voxonics\Crypto\Signer;

use Exception;
use InvalidArgumentException;
use SplFileObject;

/**
 * Class Key
 */
final class Key implements KeyInterface
{
    /**
     * @var string
     */
    private $content;

    /**
     * @var string
     */
    private $passphrase;

    /**
     * @param string        $content
     * @param string|null   $passphrase
     */
    public function __construct(string $content, string $passphrase = null)
    {
        $this->setContent($content);
        $this->passphrase = $passphrase;
    }

    /**
     * @param string $content
     *
     * @return static
     *
     * @throws InvalidArgumentException
     */
    public function setContent(string $content): self
    {
        if (strpos($content, 'file://') === 0) {
            $content = $this->readFile($content);
        }

        $this->content = $content;

        return $this;
    }

    /**
     * @param string $content
     *
     * @return string
     *
     * @throws InvalidArgumentException
     */
    private function readFile(string $content): string
    {
        try {
            $file    = new SplFileObject(substr($content, 7));
            $content = '';

            while (! $file->eof()) {
                $content .= $file->fgets();
            }

            return $content;
        } catch (Exception $exception) {
            throw new InvalidArgumentException('You must provide a valid key file', 0, $exception);
        }
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getPassphrase(): string
    {
        return $this->passphrase;
    }
}
