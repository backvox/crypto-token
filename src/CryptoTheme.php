<?php
/**
 * @link https://github.com/Aikrof
 * @package Voxonics\Crypto
 * @author Denys <AikrofStark@gmail.com>
 */

declare(strict_types=1);

namespace Voxonics\Crypto;

use JsonSerializable;
use Voxonics\Crypto\Exceptions\InvalidAlgorithmException;

/**
 * Class CryptoTheme
 */
final class CryptoTheme implements JsonSerializable
{
    /**
     * @var CryptoService
     */
    private $cryptoService;

    /**
     * @var Signature|null
     */
    private $signature;

    /**
     * CryptoTheme constructor.
     *
     * @param CryptoService $cryptoService
     */
    public function __construct(CryptoService $cryptoService)
    {
        $this->cryptoService = $cryptoService;
    }

    /**
     * @param array         $payload signature payload data
     * @param string|null   $secretKey signature secret key
     *
     * @return static
     *
     * @throws InvalidAlgorithmException
     */
    public function create(array $payload, string $secretKey = null): self
    {
        return $this->setSignature(
            $this->cryptoService->generateSignature($payload, $secretKey)
        );
    }

    /**
     * Validate current signature
     *
     * @param Signature     $expected
     * @param array         $payload
     * @param string|null   $secretKey
     *
     * @return bool
     * @throws InvalidAlgorithmException
     */
    public function validate(Signature $expected, array $payload, string $secretKey = null): bool
    {
        return $this->cryptoService->validate($expected, $payload, $secretKey);
    }

    /**
     * @return Signature|null
     */
    public function getSignature(): ?Signature
    {
        return $this->signature;
    }

    /**
     * @param Signature $signature
     *
     * @return static
     */
    public function setSignature(Signature $signature): self
    {
        $this->signature = $signature;

        return $this;
    }

    /**
     * @param string $expectedHash
     * @param array $payload
     * @param string|null $secretKey
     *
     * @return bool
     *
     * @throws InvalidAlgorithmException
     */
    public function validateHash(string $expectedHash, array $payload = [], string $secretKey = null): bool
    {
        $signature = new Signature($expectedHash);

        if (!$this->validate($signature, $payload, $secretKey)) {
            return false;
        }

        $this->setSignature($signature);

        return true;
    }

    /**
     * {@inheritDoc}
     *
     * @return array
     */
    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->signature !== null
            ? ['signature' => (string) $this->signature]
            : [];
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->signature;
    }
}